おみくじデスクトップアプリ【通知版】

## 概要

このアプリは以下サイトを参考にしたデスクトップアプリになります。
[JavaScriptでおみくじを作ろう](https://dotinstall.com/lessons/omikuji_js_v4)


## 詳細

下図参照

![おみくじ](https://bitbucket.org/andou666/omikuji-push/raw/master/img/demo.gif "おみくじ")


## 使い方

いたって普通のおみくじアプリです。運勢を占い時などにご利用ください。