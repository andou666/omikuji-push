'use strict';
{
    const btn = document.getElementById('btn');
    btn.addEventListener('click', ()=>{
      const n = Math.floor(Math.random()*3);
      //btn.textContent = n;
      
      if (n < 0.05){
          btn.textContent = '大吉';
          Push.create('大吉',{
            body: "きょう一日いいことあるかも" 
          });
      }else if(n < 0.2){
          btn.textContent = '凶';
          Push.create('凶',{
            body: "体調に気をつけよう" 
          });
      }else{
          btn.textContent = '中吉';
          Push.create('中吉',{
            body: "はい" 
          });
      }

    });
    btn.addEventListener('mousedown', ()=>{
        btn.classList.add('pressed');
    });
    btn.addEventListener('mouseup', ()=>{
        btn.classList.remove('pressed');
    });    
}